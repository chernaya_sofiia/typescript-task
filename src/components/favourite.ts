import { ApiRequest } from '../enums/api';
import { Movie } from '../interfaces/movies';

import { createElementMovie } from '../helper/createElementMovie';

import TheMovieDB from '../moviedb';
const moviedb = new TheMovieDB();


export const showFavourite = async () => {
    const favouriteNode = <HTMLElement>document.getElementById('favorite-movies');
    while (favouriteNode?.firstChild) {
        favouriteNode.removeChild(<Node>favouriteNode.lastChild);
    }

    Object.keys(localStorage).forEach(async (key) => {
        if (key !== 'undefined') {
            const movie: Movie = await moviedb.GET<Movie>(ApiRequest.MOVIE + String(key));
            createElementMovie('favorite-movies', movie);
        }
    });
};