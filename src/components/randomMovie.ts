import { Movie } from '../interfaces/movies';
import { Server } from '../enums/server';

const getRandomNumber = (max: number) => {
    return Math.floor(Math.random() * max);
}

export const showRandomMovie = (movies: Movie[]) => {
    movies = movies.filter((movie) => movie.overview && movie.backdrop_path);
    if (movies.length !== 0) {
        const randomInt = getRandomNumber(movies.length);

        const randomMovieTitle = <HTMLHeadingElement>document.getElementById('random-movie-name');
        randomMovieTitle.innerHTML = <string>movies[randomInt].title;

        const randomMovieOverview = <HTMLHeadingElement>document.getElementById('random-movie-description');
        randomMovieOverview.innerHTML = <string>movies[randomInt].overview;

        const randomMovieDiv = <HTMLDivElement>document.getElementById('random-movie');
        randomMovieDiv.style.backgroundImage = `url(${Server.BASE_IMAGE_URL + movies[randomInt].backdrop_path})`;
    }
}