export enum ApiRequest {
    POPULAR = 'movie/popular',
    TOP_RATED = 'movie/top_rated',
    UPCOMING = 'movie/upcoming',
    MOVIE = 'movie/',
    SEARCH = 'search/movie',
}