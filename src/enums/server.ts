export enum Server {
    BASE_URL = 'http://api.themoviedb.org/3/',
    BASE_IMAGE_URL = 'https://image.tmdb.org/t/p/original'
}