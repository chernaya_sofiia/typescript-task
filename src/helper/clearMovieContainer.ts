export const clearMovieContainer = (): void => {
    const movieContainer = document.getElementById('film-container');
    while (movieContainer?.firstChild) {
        movieContainer.removeChild(<Node>movieContainer.lastChild);
    }
}