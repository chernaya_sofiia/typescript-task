import { ApiRequest } from '../enums/api';
import { Movie } from '../interfaces/movies';

import TheMovieDB from '../moviedb';
const moviedb = new TheMovieDB();

export const createElementMovie = async (container: string, movie: Movie) => {
    const element = document.createElement('div');
    const elementChild = document.createElement('div');
    elementChild.classList.add('card', 'shadow-sm');

    const svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    svgElement.setAttribute('stroke', 'red');

    const imgElement = document.createElement('img');
    if (movie.poster_path === null) {
        imgElement.src = 'http://underscoremusic.co.uk/site/wp-content/uploads/2014/05/no-poster.jpg'
    }
    else {
        imgElement.src = moviedb.base_image_url + movie.poster_path;
    }

    const favouriteNode = document.getElementById('favorite-movies');

    if (container === 'favorite-movies') {
        element.classList.add('col-12', 'p-2');
        svgElement.setAttribute('id', 'f' + movie.id);
        svgElement.setAttribute('fill', 'red');
        svgElement.addEventListener('click', async () => {
            const id = svgElement.id.substring(1);
            const elemOnPage = document.getElementById(id);
            if (elemOnPage) {
                elemOnPage.setAttribute('fill', '#ff000078');
            }
            localStorage.removeItem(id);
            favouriteNode?.removeChild(<HTMLElement>svgElement.parentElement?.parentElement);
        });
    }
    else if (container === 'film-container') {
        element.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');
        svgElement.setAttribute('id', String(movie.id));

        if (localStorage.getItem(svgElement.id)) {
            svgElement.setAttribute('fill', 'red');
        }
        else {
            svgElement.setAttribute('fill', '#ff000078');
        }

        svgElement.addEventListener('click', async () => {
            if (localStorage.getItem(svgElement.id)) {
                svgElement.setAttribute('fill', '#ff000078');
                localStorage.removeItem(svgElement.id);

                const favouriteSvg = document.getElementById('f' + svgElement.id);
                const elementToDelete = favouriteSvg?.parentElement?.parentElement;
                favouriteNode?.removeChild(<HTMLElement>elementToDelete);
            }
            else {
                svgElement.setAttribute('fill', 'red');
                localStorage.setItem(svgElement.id, svgElement.id);

                const movie = await moviedb.GET<Movie>(ApiRequest.MOVIE + String(svgElement.id));
                createElementMovie('favorite-movies', movie);
            }
        });
    }

    const pathElement = document.createElementNS('http://www.w3.org/2000/svg', 'path');
    pathElement.setAttribute('d', 'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z');

    svgElement.setAttribute('width', '60');
    svgElement.setAttribute('height', '60');
    svgElement.classList.add('bi', 'bi-heart-fill', 'position-absolute', 'p-2');

    const divText = document.createElement('div');
    divText.classList.add('card-body');
    const pCardText = document.createElement('p');

    pCardText.classList.add('card-text', 'truncate');
    pCardText.innerHTML = movie.overview || 'No overview';

    const smallElement = document.createElement('small');
    smallElement.classList.add('text-muted');
    smallElement.innerHTML = movie.release_date || 'Unknown release date';

    const divFlex = document.createElement('div');
    divFlex.classList.add('d-flex', 'justify-content-between', 'align-items-center');

    divFlex.appendChild(smallElement);
    divText.appendChild(pCardText);
    divText.appendChild(divFlex);

    svgElement.appendChild(pathElement);
    elementChild.append(imgElement);
    elementChild.append(svgElement);
    elementChild.append(divText);
    element.appendChild(elementChild);

    const containerNode = document.getElementById(container);
    containerNode?.appendChild(element);
}