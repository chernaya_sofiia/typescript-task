import { ApiRequest } from '../enums/api';
import { Movies } from '../interfaces/movies';
import { createElementMovie } from '../helper/createElementMovie';
import { movieMapper } from './movieMapper';

import TheMovieDB from '../moviedb';
const moviedb = new TheMovieDB();

export const createLoadMore = (apiRequest: ApiRequest, page: number, total_page: number, query: string) => {
    const previousButton = document.getElementById('load-more');
    const newButton = <HTMLButtonElement>(previousButton?.cloneNode(true));
    previousButton?.parentElement?.appendChild(newButton);
    previousButton?.parentElement?.removeChild(previousButton);

    if ((total_page === 0) || (total_page === page)) {
        newButton.style.display = 'none';
        return;
    }
    else {
        newButton.style.display = 'block ';
    }

    newButton.addEventListener('click', async () => {
        page += 1;
        let movies: Movies;
        if (apiRequest === ApiRequest.SEARCH) {
            movies = await moviedb.GET<Movies>(apiRequest, { query: query, page: page });
        }
        else {
            movies = await moviedb.GET<Movies>(apiRequest, { page: page });
        }

        movies.results.forEach((movie) => {
            movieMapper(movie);
            createElementMovie('film-container', movie);
        });

        if (page === total_page) {
            newButton.style.display = 'none';
        }
    })
}