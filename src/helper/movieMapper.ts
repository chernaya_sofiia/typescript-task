import { Movie, RawMovie } from '../interfaces/movies';

const movieMapper = ({
    id,
    overview,
    poster_path,
    release_date,
    backdrop_path,
    title,
}: RawMovie): Movie => {
    return {
        id,
        overview,
        poster_path,
        release_date,
        backdrop_path,
        title
    };
};

export { movieMapper };
