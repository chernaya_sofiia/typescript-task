import { ApiRequest } from '../enums/api';
import { Button } from '../enums/buttons';
import { Movies } from '../interfaces/movies';

import { clearMovieContainer } from './clearMovieContainer';
import { createElementMovie } from './createElementMovie';
import { createLoadMore } from './createLoadMore';
import { showRandomMovie } from '../components/randomMovie';
import { movieMapper } from './movieMapper';

import TheMovieDB from '../moviedb';
const moviedb = new TheMovieDB();

export const showEvent = async (button: string, apiRequest: ApiRequest, page?: number) => {
    const clickedButton = document.getElementById(button);

    clickedButton?.addEventListener('click', async () => {
        let movies: Movies;
        let query = '';
        if (button === Button.SEARCH) {
            query = (<HTMLInputElement>document.getElementById('search')).value;
            movies = await moviedb.GET<Movies>(apiRequest, { query: query, page: page });
        }
        else {
            movies = await moviedb.GET<Movies>(apiRequest);
        }

        movies.results.forEach((movie) => {
            movieMapper(movie);
        });

        showRandomMovie(movies.results);
        clearMovieContainer();
        movies.results.forEach((movie) => {
            createElementMovie('film-container', movie);
        });

        createLoadMore(apiRequest, movies.page, movies.total_page, query);
    })
}