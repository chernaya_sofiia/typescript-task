
import { showEvent } from './helper/showEvent';
import { showFavourite } from './components/favourite';

import { ApiRequest } from './enums/api'
import { Button } from './enums/buttons'

export async function render(): Promise<void> {
    await showEvent(Button.POPULAR, ApiRequest.POPULAR);
    await showEvent(Button.TOP_RATED, ApiRequest.TOP_RATED);
    await showEvent(Button.UPCOMING, ApiRequest.UPCOMING);
    await showEvent(Button.SEARCH, ApiRequest.SEARCH);
    await showFavourite();

    await document.getElementById(Button.POPULAR)?.click();
}