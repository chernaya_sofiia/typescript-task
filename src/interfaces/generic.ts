export interface ErrorResponse {
    status_message: string;
    status_code: number;
}

export interface Results<T> {
    page: number;
    results: T[];
    total_page: number;
    total_results: number;
}