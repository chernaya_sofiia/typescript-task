import { Results } from './generic';

export type Movies = Results<RawMovie>;

export interface RawMovie {
  adult: boolean;
  backdrop_path: string | null;
  genre_ids: number[];
  id: number;
  original_language: string;
  original_title: string;
  overview: string;
  release_date: string;
  poster_path: string | number;
  popularity: number;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export interface Movie {
  backdrop_path: string | null;
  id: number;
  overview: string;
  release_date: string;
  poster_path: string | number;
  title: string;
}