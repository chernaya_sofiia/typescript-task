import { ErrorResponse } from './interfaces/generic';
import { Server } from './enums/server'

export default class TheMovieDB {
    private base_uri: string = Server.BASE_URL;
    private timeout: number = 5000;
    private language: string = 'en-US';

    public base_image_url: string = Server.BASE_IMAGE_URL;

    constructor(private api_key: string = 'd76f7178055b0616c341e51c700bcfef') { }

    private generateQuery = (opts: { [key: string]: string | number | boolean } = {}) => {
        const query = `?api_key=${this.api_key}&language=${this.language}`;
        return [query, ...Object.entries(opts).map(([k, v]) => `${k}=${v}`)].join('&');
    };

    private client = (
        {
            url,
            method = 'GET',
            status = 200,
            body,
            options = {},
        }: {
            url: string;
            method?: string;
            status?: number;
            body?: any;
            options?: { [key: string]: string | number | boolean };
        },
        success: (a: string) => void,
        error: (a: string) => void
    ) => {
        const xhr = new XMLHttpRequest();

        xhr.ontimeout = function () {
            error('{"status_code":408,"status_message":"Request timed out"}');
        };

        xhr.open(method, this.base_uri + url + this.generateQuery(options), true);

        if (method === 'POST') {
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.setRequestHeader('Accept', 'application/json');
        }

        xhr.timeout = this.timeout;

        xhr.onload = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === status) {
                    success(xhr.responseText);
                } else {
                    error(xhr.responseText);
                }
            } else {
                error(xhr.responseText);
            }
        };

        xhr.onerror = () => {
            error(xhr.responseText);
        };

        if (method === 'POST') {
            xhr.send(JSON.stringify(body));
        }
        else {
            xhr.send(null);
        }
    };

    public GET = <T>(url: string, options = {}) => {
        return new Promise((res: (value: T) => void, rej: (error: ErrorResponse) => void) => {
            this.client(
                { url, options },
                (v: string) => res(JSON.parse(v)),
                (e: string) => rej(JSON.parse(e))
            );
        });
    };
}